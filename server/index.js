const express = require('express');
const app = express();
const cors = require('cors');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');
const dbPath = '../db/players.json';

app.use(cors());
app.use(bodyParser.json());


/* GET home page. */
app.get('/', function (req, res, next) {
    res.send('Api is live.');
});

const getDBPath = () => {
    return path.join(__dirname, dbPath);
}

/**
 *  GET all data 
 */
app.get('/api/players', (req, res, next) => {
    fs.readFile(getDBPath(), 'utf8', (err, jsonData) => {
        if (err) {
            return res.sendStatus(404);
        }
        res.send(jsonData);
    });

});

app.listen(3000);
console.log('Running on port 3000...');