import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  allItems: any;
  pager: any = {};
  pagedItems: any[];
  prev: string;
  constructor(public rest: DataService, ) { }

  ngOnInit() {
    this.rest.getAllPitches().subscribe((data: []) => {
      this.allItems = data.map(item => {
        item['teamname'] = item['team']['name'];
        return item;
      });
      this.setPage(1);
    });
  }
  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.rest.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
  sortColumn(col: any) {
    if (col === this.prev) {
      col = "-" + col;
    }
    this.allItems.sort(dynamicSort(col));
    this.prev = col;
    this.setPage(1);
  }
  sortMultiColumn(col1: any, col2: any) {
    if (col1 === this.prev) {
      col1 = "-" + col1;
    }
    this.allItems.sort(dynamicSortMultiple(col1, col2));
    this.prev = col1;
    this.setPage(1);
  }

}

function dynamicSortMultiple(...props) {
  return function (obj1, obj2) {
    var i = 0, result = 0, numberOfProperties = props.length;
    while (result === 0 && i < numberOfProperties) {
      result = dynamicSort(props[i])(obj1, obj2);
      i++;
    }
    return result;
  }
}

function dynamicSort(property) {
  if (property)
    var sortOrder = 1;
  if (property[0] === "-") {
    sortOrder = -1;
    property = property.substr(1);
  }
  return function (a, b) {
    var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
    return result * sortOrder;
  }
}
