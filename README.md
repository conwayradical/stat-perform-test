# STAT Perform test by Conway Hyacienth
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.3.

## Prerequisites
NodeJS/npm

## Client server
From the root directory, change directory  `cd client` 
Install the client `yarn install`
Run `ng serve` for a client server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Api Server
From the root directory,change direcotry `cd server`
Install the api server `yarn install`
run `node index` for api server. navigate to `http://localhost:3000/` to load data.

## App
click on headers to re-sort data according to column